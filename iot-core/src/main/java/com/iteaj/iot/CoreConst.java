package com.iteaj.iot;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.websocket.HttpRequestWrapper;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.util.AttributeKey;

public interface CoreConst {

    /**
     * 客户端解码器
     */
    String CLIENT_DECODER_HANDLER = "ClientProtocolDecoder";

    /**
     * 客户端编码器
     */
    String CLIENT_ENCODER_HANDLER = "ClientProtocolEncoder";

    /**
     * 客户端业务处理器
     */
    String CLIENT_SERVICE_HANDLER = "ClientServiceHandler";

    /**
     * 服务端解码器
     */
    String SERVER_DECODER_HANDLER = "ServerProtocolDecoder";

    /**
     * 服务端编码器
     */
    String SERVER_ENCODER_HANDLER = "ServerProtocolEncoder";

    /**
     * 服务端业务处理器
     */
    String SERVER_SERVICE_HANDLER = "ServerServiceHandler";

    /**
     * 存活状态事件处理器
     */
    String IDLE_STATE_EVENT_HANDLER = "IdleStateEventHandler";

    /**
     * 客户端上下线、存活等事件处理器
     */
    String EVENT_MANAGER_HANDLER = "EventManagerHandler";

    /**
     * 设备编号的KEY
     */
    AttributeKey<String> EQUIP_CODE = AttributeKey.newInstance("IOT:EquipCode");

    /**
     * 设备编号的KEY
     */
    AttributeKey<FrameworkComponent> COMPONENT = AttributeKey.newInstance("IOT:Component");

    /**
     * Websocket请求对象
     */
    AttributeKey<HttpRequestWrapper> WEBSOCKET_REQ = AttributeKey.newInstance("IOT:WEBSOCKET:REQ");

    /**
     * 标识客户端连接因为重复而被关闭
     */
    AttributeKey<Boolean> CLIENT_OVERRIDE_CLOSED = AttributeKey.newInstance("IOT:CLIENT:OVERRIDE:CLOSED");


    /**
     * 标识客户端连接因为超时而被关闭
     */
    AttributeKey<Long> CLIENT_TIMEOUT_CLOSED = AttributeKey.newInstance("IOT:CLIENT:TIMEOUT:CLOSED");

    /**
     * Websocket关闭标识
     */
    AttributeKey<Boolean> WEBSOCKET_CLOSE = AttributeKey.newInstance("IOT:WEBSOCKET:CLOSE");

    /**
     * 客户端key
     */
    AttributeKey<ClientConnectProperties> CLIENT_KEY = AttributeKey.newInstance("IOT:ClientKey");

    /**
     * 客户端连接时间
     */
    AttributeKey<Long> CLIENT_ONLINE_TIME = AttributeKey.newInstance("IOT:CLIENT:ONLINE:TIME");


    /**
     * 标识客户端是正常关闭(主动关闭)
     */
    AttributeKey<Boolean> CLIENT_CLOSED_NORMAL = AttributeKey.newInstance("IOT:CLIENT:CLOSE:NORMAL");

    /**
     * 标识客户端关闭类型(1.关闭 2.断开)
     */
    AttributeKey<Integer> CLIENT_CLOSED_TYPE = AttributeKey.newInstance("IOT:CLIENT:CLOSE:TYPE");
}
