package com.iteaj.iot.format;

/**
 * CDAB格式
 * @see DataFormat#CDAB
 */
public class CDABFormat extends DataFormatConvert{

    public static CDABFormat INSTANCE = new CDABFormat();

    protected CDABFormat() { }

    @Override
    protected byte[] byte2Transform(byte[] data, int offset) {
        byte[] buffer = new byte[2];
        buffer[0] = data[offset + 0];
        buffer[1] = data[offset + 1];
        return buffer;
    }

    @Override
    protected byte[] byte4Transform(byte[] data, int offset) {
        byte[] buffer = new byte[4];
        buffer[0] = data[offset + 2];
        buffer[1] = data[offset + 3];
        buffer[2] = data[offset + 0];
        buffer[3] = data[offset + 1];
        return buffer;
    }

    @Override
    protected byte[] byte8Transform(byte[] data, int offset) {
        byte[] buffer = new byte[8];
        buffer[0] = data[offset + 6];
        buffer[1] = data[offset + 7];
        buffer[2] = data[offset + 4];
        buffer[3] = data[offset + 5];
        buffer[4] = data[offset + 2];
        buffer[5] = data[offset + 3];
        buffer[6] = data[offset + 0];
        buffer[7] = data[offset + 1];
        return buffer;
    }
}
