package com.iteaj.iot.client.http.impl;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.http.HttpClient;
import com.iteaj.iot.client.http.HttpClientComponent;
import com.iteaj.iot.client.http.HttpClientConnectProperties;
import com.iteaj.iot.client.http.HttpInterceptor;
import com.iteaj.iot.client.http.okhttp.OkHttpClient;

/**
 * 默认的Http协议客户端实现
 */
public class DefaultHttpClientComponent extends HttpClientComponent<DefaultHttpClientMessage> {

    private HttpClient defaultClient;

    public DefaultHttpClientComponent() { }

    public DefaultHttpClientComponent(HttpClientConnectProperties connectProperties) {
        this(connectProperties, null);
    }

    public DefaultHttpClientComponent(HttpClientConnectProperties connectProperties, HttpInterceptor interceptor) {
        super(connectProperties, interceptor);
        if(connectProperties != null) {
            defaultClient = createNewClient(connectProperties);
        }
    }

    @Override
    public String getName() {
        return "HttpClient";
    }

    @Override
    public String getDesc() {
        return "默认Http客户端实现";
    }

    @Override
    public HttpClient getClient() {
        return defaultClient;
    }

    /**
     * 默认使用OKHttp3
     * @param config
     * @return
     */
    @Override
    protected HttpClient doCreateClient(ClientConnectProperties config) {
        return new OkHttpClient((HttpClientConnectProperties) config, this);
    }
}
