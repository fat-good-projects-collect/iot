package com.iteaj.iot.client.http;

import com.iteaj.iot.client.ClientConnectProperties;

import java.net.URI;

public class HttpClientConnectProperties extends ClientConnectProperties {

    public HttpClientConnectProperties(String url) {
        this(URI.create(url));
    }

    public HttpClientConnectProperties(URI uri) {
        this(uri.getHost(), uri.getPort());
    }

    public HttpClientConnectProperties(String remoteHost, Integer remotePort) {
        super(remoteHost, remotePort, String.format("%s:%s", remoteHost, remotePort.toString()));
    }
}
