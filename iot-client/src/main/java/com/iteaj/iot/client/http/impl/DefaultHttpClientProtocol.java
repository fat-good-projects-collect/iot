package com.iteaj.iot.client.http.impl;

import com.iteaj.iot.client.http.HttpClientProtocol;
import okhttp3.RequestBody;

import java.util.Map;

public class DefaultHttpClientProtocol extends HttpClientProtocol<DefaultHttpClientMessage> {

    protected DefaultHttpClientProtocol(DefaultHttpClientMessage requestMessage) {
        super(requestMessage);
    }

    public static DefaultHttpClientProtocol get(String url) {
        return new DefaultHttpClientProtocol(DefaultHttpClientMessage.get(url));
    }

    public static DefaultHttpClientProtocol get(String url, Map<String, Object> query) {
        return new DefaultHttpClientProtocol(DefaultHttpClientMessage.get(url, query));
    }

    public static DefaultHttpClientProtocol post(String url) {
        return new DefaultHttpClientProtocol(DefaultHttpClientMessage.post(url));
    }

    public static DefaultHttpClientProtocol post(String url, Map<String, Object> query) {
        return new DefaultHttpClientProtocol(DefaultHttpClientMessage.post(url, query));
    }

    public static DefaultHttpClientProtocol post(String url, Map<String, Object> query, Map<String, String> body) {
        return new DefaultHttpClientProtocol(DefaultHttpClientMessage.post(url, query, body));
    }

    public static DefaultHttpClientProtocol post(String url, Map<String, Object> query, RequestBody body) {
        return new DefaultHttpClientProtocol(DefaultHttpClientMessage.post(url, query, body));
    }

    @Override
    protected DefaultHttpClientMessage doBuildRequestMessage() {
        return this.requestMessage();
    }

    @Override
    protected DefaultHttpClientMessage doBuildResponseMessage(DefaultHttpClientMessage message) {
        return message;
    }

    @Override
    public <T> T protocolType() {
        return null;
    }
}
