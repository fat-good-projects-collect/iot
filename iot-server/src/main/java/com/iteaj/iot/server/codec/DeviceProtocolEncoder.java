package com.iteaj.iot.server.codec;

import com.iteaj.iot.*;
import com.iteaj.iot.event.ExceptionEvent;
import com.iteaj.iot.server.ServerSocketProtocol;
import com.iteaj.iot.server.TcpServerComponent;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Create Date By 2017-09-08
 *  平台协议编码器, 线程安全
 * @author iteaj
 * @since 1.7
 */
@ChannelHandler.Sharable
public class DeviceProtocolEncoder extends MessageToMessageEncoder<ServerSocketProtocol> {

    private Logger logger = LoggerFactory.getLogger(DeviceProtocolEncoder.class);

    private static DeviceProtocolEncoder deviceProtocolEncoder = new DeviceProtocolEncoder();
    protected DeviceProtocolEncoder() { }

    public static DeviceProtocolEncoder getInstance() {
        return deviceProtocolEncoder;
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, ServerSocketProtocol protocol, List<Object> out) throws Exception {

        try {
            TcpServerComponent serverComponent = (TcpServerComponent) ctx.channel().attr(CoreConst.COMPONENT).get();
            //平台主动请求的则写出请求报文
            if(protocol instanceof ProtocolPreservable) {
                SocketMessage requestMessage = protocol.requestMessage();
                // 构建方法必须再放入协议工厂之前调用
                SocketMessage message = doBuild(requestMessage, "平台请求客户端", serverComponent);

                //如果需要保存映射关系,用relationKey作为关联映射
                if(((ProtocolPreservable) protocol).isRelation()){
                    final long timeout = ((ProtocolPreservable) protocol).getTimeout();
                    serverComponent.protocolFactory().add((String) ((ProtocolPreservable) protocol).relationKey(), protocol, timeout);
                }

                out.add(Unpooled.wrappedBuffer(message.getMessage()));

                //如果是设备请求的则写出响应报文
            } else if(protocol instanceof ClientInitiativeProtocol) {

                Message message = doBuild(protocol.responseMessage(), "平台响应客户端", serverComponent);
                out.add(Unpooled.wrappedBuffer(message.getMessage()));
            } else {
                logger.error("平台报文编码({}) 不支持的协议类型", serverComponent.getName(), new ProtocolException("不支持的协议"));
            }
        } catch (Exception exception) {
            logger.error("平台报文编码 编码异常({}) - 设备编号：{} - 协议类型：{} - 已发送异常事件[ExceptionEvent]", exception.getCause()
                    , protocol.getEquipCode(), protocol.protocolType(), exception);
            FrameworkManager.publishEvent(new ExceptionEvent(exception, protocol.getEquipCode()));
        }

    }

    protected SocketMessage doBuild(SocketMessage message, String desc, TcpServerComponent serverComponent) throws IOException {
        if(message.getMessage() == null) {
            message.writeBuild();
        }

        if(logger.isTraceEnabled()) {
            Message.MessageHead head = message.getHead();
            logger.trace("{}({}) 客户端编号: {} - 协议类型: {} - messageId: {} - 报文: {}", desc
                    , serverComponent.getName(), head.getEquipCode(), head.getType(), head.getMessageId(), message);
        }
        return message;
    }
}
